#include<iostream>  
     
using namespace std;

template <class T>

class Compare                     
{
public:
	Compare(T a,T b);

	T max();

	T min();

private:
	T x, y;
};

template <typename T>
Compare<T>::Compare(T a, T b)
{
    this->x = a;
    this->y = b;
}

template <typename T>
T Compare<T>::max()
{
	return (x > y ? x : y);
}

template <typename T>
T Compare<T>::min()
{
	return (x < y ? x : y);
}

int main()
{
	Compare <int> cmp1(3, 7);                    
	cout << cmp1.max() << "is the Maximum of two integer numbers." << endl;
	cout << cmp1.min() << "is the Minimum of two integer numbers." << endl << endl;

	Compare <double> cmp2(45.78, 93.6);
	cout << cmp2.max() << "is the Maximum of two integer numbers." << endl;
    cout << cmp2.min() << "is the Minimum of two integer numbers." << endl << endl;

	Compare <char> cmp3('a', 'A');
	cout << cmp3.max() << "is the Maximum of two integer numbers." << endl;
	cout << cmp3.min() << "is the Minimum of two integer numbers." << endl << endl;

	return 0;
}


