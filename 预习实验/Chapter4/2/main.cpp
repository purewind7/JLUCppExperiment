#include<iostream>


using namespace std;

class Complex
{
public:
	Complex() 
	{ 
		real = 0; 
		imag = 0; 
	}
	Complex(double r, double i)
	{ 
		real = r; 
		imag = i;
	}
	
	Complex operator+(Complex& c2);
	Complex operator+(int &c2); 
	friend Complex operator+(int &c1,Complex &c2); 
	
	void display()
	{
		cout << real << "," << imag << endl;
	}
	
private:
	double real;
	double imag;	
};
Complex Complex::operator +(Complex& c2)
{
	Complex c;
	c.real = real + c2.real;
	c.imag = imag + c2.imag;
	return c;
}
Complex Complex::operator +(int& c2)
{
	Complex c;
	c.real = real + c2;
	c.imag = imag;
	return c;
}
Complex operator+(int &c1,Complex &c2)
{
	Complex c;
	c.real = c1 + c2.real;
	c.imag = c2.imag;
	return c;
}
int main()
{
	Complex a(3,4);
	Complex b(6,6);
	Complex c= a + b ;
	c.display();
	int i=3;
	Complex d=a+i;
	d.display();
	Complex e=i+a;
	e.display();
		
}
