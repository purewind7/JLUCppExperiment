#include<iostream>
#include<string>

using namespace std;

class Teacher
{
public:

	Teacher(string,int ,int ,int ,int ,int );
	
	void display();
	
private:
	
	string name;
	
	int age;
	int sex;
	int addr;
	int number;
	int title;
		
};
Teacher::Teacher(string n, int a, int s, int add, int t, int ti) :name(n), age(a), sex(s), addr(add), number(t),title(ti) {};

class Cadre
{
public:
	Cadre(string,int ,int ,int ,int ,int );

	int post;
private:

	string name;
	
	int age;
	int sex;
	int addr;
	int number;
	
};

Cadre::Cadre(string n, int a, int s, int add, int t, int p) :name(n), age(a), sex(s), addr(add),number(t),post(p) {};

void Teacher::display()
{
	
	cout << "name:" << name << endl;
	cout << "age:" << age << endl;
	cout << "sex:" << sex << endl;
	cout << "title:" << title << endl;
	cout << "addr:" << addr << endl;
	cout << "number:" << number << endl;
	
}

class Teacher_Cadre : public Teacher,public Cadre
{
	
public:
	Teacher_Cadre(string n, int a, int s, int add, int t, int p, int ti, int wa);

private:	

	int wages;
	
	void show();
	
	
};

void Teacher_Cadre::show()
{
	
	Teacher::display();
	
	cout << "post:" << post << endl;
	cout << "wages:" << wages << endl;
	
}

Teacher_Cadre::Teacher_Cadre(string n, int a, int s, int add, int t, int p, int ti, int wa) :
	Teacher(n, a, s, add, t, ti), Cadre(n, a, s, add, t, p), wages(wa) {};


int main()
{
	Teacher_Cadre TC("aa",1,2,3,4,5,6,7);
	
	TC.show();
	
	return 0;
	
} 
