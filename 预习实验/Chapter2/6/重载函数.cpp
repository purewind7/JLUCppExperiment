#include <iostream>
#include<algorithm>
using namespace std;


void sort(int* num, int len)
{
	sort(num,num+len);
}

void sort(double* num, int len)
{
	sort(num,num+len);
}

void sort(float* num, int len)
{
	sort(num,num+len);
}

int main()
{
    int* num, n = 0;
	 
    cout << "排序n个数据: " << endl;
    cin >> n;
    if (n <= 0)
    {
        cout << "intput err!" << endl;
        return -1;
    }
    num = new int[n];

    cout << "请输入n个数: " << endl;
    for (int i = 0; i < n; i++)
    {
        cin >> num[i];
 	}
 	
    sort(num, n);

    cout << "排序结果为: ";
    for (int j = 0; j < n; j++)
    {
		cout << num[j] << ' ';
	}    
    cout << endl;
    
    delete[] num;

    return 0;
}
