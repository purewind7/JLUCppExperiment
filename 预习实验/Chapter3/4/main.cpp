#include<iostream>
using namespace std;

class Student
{
public:
	
	Student(int n, float s) :num(n), score(s) {}
	
	void change(int n, float s) 
	{
		num = n;
		score = s;
	}
	
	void display() 
	{ 
		cout << num << " " << score << endl; 
	}
	
	friend void fun(Student&);
	
private:
	int num;
	float score;
};

void fun(Student& s)
{
	s.change(s.num, s.score);
	s.display();
}

int main()
{
	Student stud1(101, 78.5);
	stud1.display();
	Student stud2(101, 80.5);
	fun(stud2);
	return 0;
}
