#include <iostream> 
#include <string> 

using namespace std;

class Student
{
public:
	void display();
	void get_value();
protected:
	int num;
	string name;
	string sex;
};

void Student::get_value()
{

	cin >> num;
	cin >> name;
	cin >> sex;
	
}

void Student::display()
{
	cout << "num: " << num << endl; cout << "name: " << name << endl; cout << "sex: " << sex << endl;
}

class Student1 : protected Student
{
public:
	
	void display_();
	void get_value_();
	
private:
	int age;
	string addr;
};

void Student1::get_value_()
{
	get_value(); 
	cin >> age;
	cin >> addr;
}

void Student1::display_()
{
	display();
	cout << "age: " << age << endl;
	cout << "addr: " << addr << endl;
}

int main()
{
	Student1 stu;
	
	stu.get_value_();
	stu.display_();
	return 0;
}
