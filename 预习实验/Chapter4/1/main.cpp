#include<iostream>


using namespace std;

class Complex
{
public:
	Complex() 
	{ 
		real = 0; 
		imag = 0; 
	}
	Complex(double r, double i)
	{ 
		real = r; 
		imag = i;
	}
	
	Complex operator+(Complex& c2);
	Complex operator-(Complex& c2);
	Complex operator*(Complex& c2);
	Complex operator/(Complex& c2);
	
	void display()
	{
		cout << "(" << real << "," << imag << "i)" << endl;
	}
	
private:
	double real;
	double imag;	
};
Complex Complex::operator +(Complex& c2)
{
	Complex c;
	c.real = real + c2.real;
	c.imag = imag + c2.imag;
	return c;
}
Complex Complex::operator -(Complex& c2)
{
	Complex c;
	c.real = real - c2.real;
	c.imag = imag - c2.imag;
	return c;
}
Complex Complex::operator *(Complex& c2)
{
	Complex c;
	c.real = real * c2.real;
	c.imag = imag * c2.imag;
	return c;
}
Complex Complex::operator /(Complex& c2)
{
	Complex c;
	c.real = (real * c2.real + imag * c2.imag) / (c2.real * c2.real + c2.imag * c2.imag);
	c.imag = (imag * c2.real - real * c2.imag) / (c2.real * c2.real + c2.imag * c2.imag);
	return c;
}

int main()
{
	Complex a(3,4);
	Complex b(6,6);
	Complex c= a + b ;
	c.display();
	Complex c= a - b ;
	c.display();	
	Complex c= a * b ;
	c.display();	
	Complex c= a / b ;
	c.display();
		
}
