#include <iostream>
#include <string>

using namespace std;

class Student
{
public:
	
	void display()
	{
		cout << "num: " << num << endl;
		cout << "name: " << name << endl;
		cout << "sex: " << sex << endl;
	}
protected:
	int num;
	string name;
	char sex;
};
class Student1 : protected Student
{
public:
	
	void get_value_() ;
	void display_();

private:
	int age;
	string addr;
};

void Student1::display_()
{
	display();
	cout << "age: " << age << endl;
	cout << "addr: " << addr << endl;
}

void Student1::get_value_()
{
	cin >> num >> name >> sex >> age >> addr;
}

int main()
{
	Student1 stud;
	
	stud.get_value_();
	stud.display_();

	return 0;
}
