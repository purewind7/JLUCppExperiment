#include <iostream>
#include <fstream>
#include <stdlib.h>
#include<algorithm>

using namespace std;

void fun1()
{
    int a[10];
    ofstream outfile1("f1.dat"), outfile2("f2.dat");

    cout << "input ten numbers:" << endl;
    for (int i = 0; i < 10; i++)
    {
        cin >> a[i];
        outfile1 << a[i] << " ";
    }

    cout << "input ten numbers:" << endl;
    for (int i = 0; i < 10; i++)
    {
        cin >> a[i];
        outfile2 << a[i] << " ";
    }

    outfile1.close();
    outfile2.close();
}


void fun2()
{
    ifstream infile("f1.dat");

    ofstream outfile("f2.dat", ios::app);

    int a;

    for (int i = 0; i < 10; i++)
    {
        infile >> a;
        outfile << a << " ";
    }
    infile.close();
    outfile.close();

}



void fun3()
{
    ifstream infile("f2.dat");

    int a[20];
    int i, j, t;
    for (i = 0; i < 20; i++)
        infile >> a[i];

    sort(a, a + 20);
    infile.close();
    ofstream outfile("f2.dat", ios::out);

    if (!outfile)
    {
        cerr << "open f2.dat error" << endl;
        exit(1);
    }

    cout << "f2.dat:" << endl;

    for (i = 0; i < 20; i++)
    {
        outfile << a[i] << " ";
        cout << a[i] << " ";
    }
    cout << endl;
    outfile.close();
}

int main()
{

    fun1();
    fun2();
    fun3();
    
    return 0;
}