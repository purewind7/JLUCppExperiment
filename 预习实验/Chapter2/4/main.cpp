#include<iostream>
#include<algorithm>

using namespace std;

void sort1(int &a,int &b,int &c)
/***************************************************************
 *  @brief     使用引用的方法对三个数排序 
 *  @param     三个待排序的数   
 *  @note      
 *  @Sample usage:     
 **************************************************************/
{
    int temp[3];
    temp[0]=a;
    temp[1]=b;
    temp[2]=c;
    
    sort(temp,temp+3);
	
	a=temp[0];
	b=temp[1];
	c=temp[2];
	
}

void sort2(int *a,int *b,int *c)
/***************************************************************
 *  @brief     使用指针的方法对三个数排序 
 *  @param     三个待排序的数   
 *  @note      
 *  @Sample usage:     
 **************************************************************/
{
	int temp[3];
    temp[0]=*a;
    temp[1]=*b;
    temp[2]=*c;
    
    sort(temp,temp+3);
    
    *a=temp[0];
    *b=temp[1];
    *c=temp[2];
    
}


int main()
{
	int a, b, c;

	cin >> a >> b >> c;
	sort1(a, b, c);//使用引用 
	cout << a << b << c << endl;
	
	cin >> a >> b >> c;
	sort2(&a,&b,&c);//使用指针 
	cout << a << b << c << endl;

	return 0;

}
